const odataApi = require("./api");

const api = new odataApi();

const entity = async (entityName, { list }) => {
  if (list) {
    const resp = await api.getEntities();
    console.log("Available entity :");
    resp.forEach((element) => {
      console.log(` - ${element}.`);
    });
  } else if (entityName) {
    const resp = await api.getEntity(entityName);
    console.log(`List of ${entityName}:`);
    resp.forEach(console.log);
  }
};

const people = async (personName) => {
  if (personName) {
    const resp = await api.getPerson(personName);
    console.log(`Detail person of ${personName}:`);
    console.log(resp);
  }
};

module.exports = {
  entity,
  people,
};
