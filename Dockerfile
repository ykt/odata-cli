FROM node:17-alpine

COPY package.json yarn.lock ./.

RUN yarn 

COPY . . 
ENTRYPOINT node index.js $@
CMD node index.js $@
