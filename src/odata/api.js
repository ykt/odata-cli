const axios = require("axios");
class ODataApi {
  url = "https://services.odata.org/TripPinRESTierService";

  getUrl() {
    return this.url;
  }
  async getEntities() {
    const resp = await this._get(this.url);
    return resp.data["value"].map((item) => item.name);
  }
  async getEntity(entityName) {
    const resp = await this._get(`${this.url}/${entityName}`);
    return resp.data["value"];
  }
  async getPerson(personName) {
    const resp = await this._get(`${this.url}/People('${personName}')`);
    return resp.data;
  }

  async _get(url) {
    return axios(url).catch(({ response }) => {
      if (response) {
        const {
          status,
          data: {
            error: { message },
          },
        } = response;
        console.log(`Error found: (${status}) - ${message} `);
        process.exit(1);
      }
      throw error;
    });
  }
}
module.exports = ODataApi;
