#!/usr/bin/env node

const cli = require("commander");
const odata = require("./src/odata");
cli.description("OData API cli");

cli
  .command("entity")
  .option("-l, --list", "List all define entity.")
  .argument("[entityName]", "List specific define entity of entity Name.")
  .description("Retrieve a list of all available entity")
  .action(odata.entity);

cli
  .command("show")
  .argument("[name]", "Show detail of specific person")
  .description("Show detail of person")
  .action(odata.people);

cli.parse(process.argv);
